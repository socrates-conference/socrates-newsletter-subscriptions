process.env.SUBSCRIBERS_TABLE = 'subscribers'
import {APIGatewayProxyEvent} from 'aws-lambda'
import {getSubscribers} from './getSubscribers'
import {
  DynamoSubscriber,
  FakeDynamoDB
} from '../mock/dynamoDB'
import {DynamoDB} from '@aws-sdk/client-dynamodb'


describe('GET /subscribers', () => {
  let fakeDynamoDB: FakeDynamoDB
  beforeEach(() => {
    fakeDynamoDB = new FakeDynamoDB()
  })

  describe('without query parameters', () => {
    it('should return empty list', async () => {
      const result = await getSubscribers(fakeDynamoDB as unknown as DynamoDB)({queryStringParameters: undefined} as APIGatewayProxyEvent)
      expect(result.statusCode).toEqual(200)
      expect(fakeDynamoDB.table).toEqual('subscribers')
      expect(JSON.parse(result.body)).toEqual([])
    })
    describe('when a subscriber exists', () => {
        const SUB: DynamoSubscriber = {id:{S:"1"}, name:{S:"somebody"},email:{S:"some@body.com"}}
      beforeEach(()=>{
        fakeDynamoDB._items.set(JSON.stringify({id:{S:"1"}}), SUB)
      })

      it('should return list of one', async () => {
        const result = await getSubscribers(fakeDynamoDB as unknown as DynamoDB)({} as APIGatewayProxyEvent)
        expect(result.statusCode).toEqual(200)
        expect(fakeDynamoDB.table).toEqual('subscribers')
        expect(JSON.parse(result.body)).toEqual([{id:"1", name:"somebody", email:"some@body.com"}])
      })
    })
  })

  describe('with query parameters', () => {
    it('should return 404 error', async () => {
      const result = await getSubscribers(fakeDynamoDB as unknown as DynamoDB)({queryStringParameters: {"id":"1"}}as unknown as APIGatewayProxyEvent)
      expect(result.statusCode).toEqual(404)
      expect(fakeDynamoDB.table).toEqual('subscribers')
      expect(JSON.parse(result.body).reason).toEqual("A subscriber with id:1 does not exist")
    })
    describe('when a subscriber exists', () => {
      const SUB: DynamoSubscriber = {id:{S:"1"}, name:{S:"somebody"},email:{S:"some@body.com"}}
      beforeEach(()=>{
        fakeDynamoDB._items.set(JSON.stringify({id:{S:"1"}}), SUB)
      })

      it('should return single result element', async () => {
        const result = await getSubscribers(fakeDynamoDB as unknown as DynamoDB)({queryStringParameters: {"id":"1"}}as unknown as APIGatewayProxyEvent)
        expect(result.statusCode).toEqual(200)
        expect(fakeDynamoDB.table).toEqual('subscribers')
        expect(JSON.parse(result.body)).toEqual({id:"1", name:"somebody", email:"some@body.com"})
      })
    })
  })
})