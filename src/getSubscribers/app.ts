import {getSubscribers} from './getSubscribers'
import {DynamoDB} from '@aws-sdk/client-dynamodb'

export const handler = getSubscribers(new DynamoDB({region: 'eu-central-1'}))