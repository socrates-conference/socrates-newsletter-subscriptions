import {
  APIGatewayProxyEvent,
  APIGatewayProxyResult
} from 'aws-lambda'
import {
  DynamoDB,
  GetItemCommandOutput,
  ScanCommandOutput
} from '@aws-sdk/client-dynamodb'
import {Subscriber} from '../common/types'
import {
  failure,
  success,
  tableName
} from '../common/util'

const fromItem = (item): Subscriber => {
  const subscriber = {}
  Object.entries(item).forEach(([key, value]) => subscriber[key] = value[Object.keys(value)[0]])
  return subscriber as Subscriber
}

export const getSubscribers = (dynamoDB: DynamoDB) => async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
  const id: string | undefined = event?.queryStringParameters?.id
  if (id !== undefined) {
    const result: GetItemCommandOutput = await dynamoDB.getItem({TableName: tableName, Key: {'id': {'S': id}}})
    if(result?.Item !== undefined) return success(fromItem(result.Item))
    else return failure(404, `A subscriber with id:${id} does not exist`)
  } else {
    const result: ScanCommandOutput = await dynamoDB.scan({TableName: tableName})
    return success(result.Items.map(fromItem))
  }
}