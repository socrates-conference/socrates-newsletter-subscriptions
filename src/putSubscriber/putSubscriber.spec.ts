import {FakeSES} from '../mock/ses'
import {APIGatewayProxyResult} from 'aws-lambda'
import {DynamoDB} from '@aws-sdk/client-dynamodb'
import {putSubscriber} from './putSubscriber'
import {FakeDynamoDB} from '../mock/dynamoDB'
import {SES} from '@aws-sdk/client-ses'

process.env.SUBSCRIBERS_TABLE = 'subscribers'


describe('PUT /subscribers', () => {
  let result: APIGatewayProxyResult
  const dynamoDB: FakeDynamoDB = new FakeDynamoDB()
  const ses: FakeSES = new FakeSES()
  const event: any = {body: JSON.stringify({consentKey: '123456789'})}

  describe('when there is no request body', () => {
    beforeEach(async () => {
      result = await putSubscriber(ses as unknown as SES, dynamoDB as unknown as DynamoDB)({} as any)
    })
    it('should return 400 error', ()=> {
      expect(result.statusCode).toEqual(400)
      expect(JSON.parse(result.body).reason).toEqual('The input arguments were invalid.')
    })
  })

  describe('when there is no consent key', () => {
    beforeEach(async () => {
      result = await putSubscriber(ses as unknown as SES, dynamoDB as unknown as DynamoDB)({body:"{}"} as any)
    })
    it('should return 400 error', ()=> {
      expect(result.statusCode).toEqual(400)
      expect(JSON.parse(result.body).reason).toEqual('The request is missing the required consent key.')
    })
  })

  describe('when there is no match for the given consent key', () => {
    beforeEach(async () => {
      result = await putSubscriber(ses as unknown as SES, dynamoDB as unknown as DynamoDB)(event)
    })
    it('should return 404 error', ()=> {
      expect(result.statusCode).toEqual(404)
      expect(JSON.parse(result.body).reason).toEqual('The consent key you provided does not match any on record.')
    })
  })

  describe('when there is a match for the given consent key', () => {
      let key: string = JSON.stringify({id: {S:"1234"}})
    beforeEach(async () => {
      dynamoDB._items.set(key, {id: {S:"1234"}, name: {S:"Jane Doe"}, email:{S:"jane@doe.com"}, consentKey:{S:"123456789"}})
      result = await putSubscriber(ses as unknown as SES, dynamoDB as unknown as DynamoDB)(event)
    })
    it('should return 200', ()=> {
      expect(result.statusCode).toEqual(200)
    })
    it('should remove the consent key from the stored item', ()=>{
      expect(dynamoDB._items.get(key).consentKey).toBeUndefined()
    })
  })
})