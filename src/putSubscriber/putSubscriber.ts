import {
  APIGatewayProxyEvent,
  APIGatewayProxyResult
} from 'aws-lambda'
import {DynamoDB} from '@aws-sdk/client-dynamodb'
import {
  failure,
  success,
  tableName
} from '../common/util'
import {SES} from '@aws-sdk/client-ses'

async function removeConsentKey(item, dynamoDB: DynamoDB): Promise<void> {
  item.consentKey = undefined
  delete item.consentKey
  await dynamoDB.putItem({Item: item, TableName: tableName})
}

async function sendConfirmationEmail(item, ses: SES): Promise<void> {
  const TemplateData = `{"name":"${item.name.S}, "email":"${item.email.S}"`
  const Template: string = process.env.CONFIRMATION_TEMPLATE
  const Source: string = process.env.SOURCE_EMAIL
  const Destination = {ToAddresses: [item.email.S]}
  console.log('Sending confirmation email\n\n%s\n%s\n%o\n%s\n\n', Template, Source, Destination, TemplateData)
  await ses.sendBulkTemplatedEmail({
    Template,
    Source,
    Destinations: [{
      Destination,
      ReplacementTemplateData: TemplateData
    }],
    DefaultTemplateData: TemplateData,
    ConfigurationSetName: 'Standard'
  })
}

export const putSubscriber = (ses: SES, dynamoDB: DynamoDB) => async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
  try {
    const {consentKey} = JSON.parse(event.body)
    if (!consentKey) {
      return failure(400, 'The request is missing the required consent key.')
    }
    const existing = await dynamoDB.scan({
      FilterExpression: "consentKey = :consentKey",
      ExpressionAttributeValues: {":consentKey": {S: consentKey}},
      TableName: tableName
    })
    if (existing.Items !== undefined && existing.Items.length > 0) {
      if (existing.Items.length > 1) {
        return failure(400, 'The given consent key gave too many results - perhaps you provided a partial value?')
      }
      const item = existing.Items[0]
      await removeConsentKey(item, dynamoDB)
      await sendConfirmationEmail(item, ses)
      return success()
    } else {
      return failure(404, 'The consent key you provided does not match any on record.')
    }
  } catch (e) {
    console.log('error while confirming subscription: \n %o', e)
    return e.type === 'ValidationError' || e.message.startsWith('Unexpected token')
           ? failure(400, 'The input arguments were invalid.')
           : failure(500, e.message)
  }
}