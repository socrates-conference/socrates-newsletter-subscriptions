import {putSubscriber} from './putSubscriber'
import {DynamoDB} from '@aws-sdk/client-dynamodb'
import {SES} from '@aws-sdk/client-ses'

export const handler = putSubscriber(new SES({region:'eu-central-1'}), new DynamoDB({region: 'eu-central-1'}))