import {
  ScanCommandInput,
  ScanOutput
} from '@aws-sdk/client-dynamodb'

type DynamoString = { S: string }
type DynamoKey = { id: DynamoString }
export type DynamoSubscriber = { id: DynamoString, name: DynamoString, email: DynamoString, consentKey?: DynamoString }

export class FakeDynamoDB {

  public _items: Map<string, DynamoSubscriber> = new Map()
  public table: String

  async putItem(props: { TableName: string, Item: DynamoSubscriber }) {
    const {TableName, Item} = props
    this.table = TableName
    this._items.set(JSON.stringify({'id': Item.id}), Item)
  }

  async getItem(props: { TableName: string, Key: DynamoKey }) {
    const {TableName, Key} = props
    this.table = TableName
    const Item: DynamoSubscriber = this._items.get(JSON.stringify(Key))
    return {Item}
  }

  async deleteItem(props: { TableName: string, Key: DynamoKey }) {
    const {TableName, Key} = props
    this.table = TableName
    const serializedKey: string = JSON.stringify(Key)
    if (this._items.has(serializedKey)) {
      return this._items.delete(serializedKey)
    } else {
      const error: Error = new Error()
      // @ts-ignore
      error.__type = 'com.amazonaws.dynamodb.v20120810#ResourceNotFoundException'
      error.message = `Requested resource not found: Item: ${Key.id} not found`
      throw error
    }
  }

  async scan({TableName, FilterExpression, ExpressionAttributeValues}: ScanCommandInput): Promise<ScanOutput> {
    this.table = TableName

    const subscribers: DynamoSubscriber[] = [...this._items.values()]
    const scanResult: DynamoSubscriber[] = FilterExpression
                                           ? subscribers
                                             .filter(s =>
                                               Object.keys(ExpressionAttributeValues).reduce((all, key) => {
                                                 const attribute: string = key.substr(1)
                                                 return all && s[attribute]?.S !== undefined && s[attribute].S !== ''
                                                   && s[attribute].S === ExpressionAttributeValues[key].S
                                               }, true))
                                           : [...subscribers]
    return {Items: scanResult as any[]}
  }
}