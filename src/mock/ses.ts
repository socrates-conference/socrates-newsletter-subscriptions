import {
  SendBulkTemplatedEmailCommandInput,
  SendBulkTemplatedEmailCommandOutput
} from '@aws-sdk/client-ses'
import {BulkEmailDestination} from '@aws-sdk/client-ses/models/models_0'


export class FakeSES {
  public source: string
  public template: String
  public destinations: BulkEmailDestination[]

  public sendBulkTemplatedEmail = (options: SendBulkTemplatedEmailCommandInput): SendBulkTemplatedEmailCommandOutput => {
    this.template = options.Template
    this.source = options.Source
    this.destinations = options.Destinations
    return {} as SendBulkTemplatedEmailCommandOutput
  }
}