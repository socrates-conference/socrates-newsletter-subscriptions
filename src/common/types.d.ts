export type UUID = string
export type Subscriber = {
  name: string,
  email: string
  consentKey: string
}