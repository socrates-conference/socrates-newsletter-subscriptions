import {DynamoDB} from '@aws-sdk/client-dynamodb'
import {
  APIGatewayProxyEvent,
  APIGatewayProxyResult
} from 'aws-lambda'
import {
  failure,
  success,
  tableName
} from '../common/util'

export const deleteSubscriber = (dynamoDB: DynamoDB) => async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
  const email = event?.pathParameters?.email
  if (email !== undefined) {
    try {
      const existing = await dynamoDB.scan({
        FilterExpression: "email = :email",
        ExpressionAttributeValues: {":email": {S: email}},
        TableName: tableName
      })
      if(!existing.Items || existing.Items.length === 0)
        return failure(404, 'The specified email does not exist.')
      else if( existing.Items.length > 1 )
        return failure(500, 'There was more than one existing entry with the specified email address. This is likely due to a technical problem. Please contact the organizers.')
      else {
        await dynamoDB.deleteItem({TableName: tableName, Key: {id: {S: existing.Items[0].id.S as unknown as string}}})
        return success()
      }
    } catch (e) {
      return e.__type?.endsWith('ResourceNotFoundException')
             ? failure(404, e.message)
             : failure(500, e.message)
    }
  } else {
    return failure(400, 'Missing required parameter email')
  }
}