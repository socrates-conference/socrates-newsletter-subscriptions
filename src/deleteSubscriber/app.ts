import {deleteSubscriber} from './deleteSubscriber'
import {DynamoDB} from '@aws-sdk/client-dynamodb'

export const handler = deleteSubscriber(new DynamoDB({region: 'eu-central-1'}))
