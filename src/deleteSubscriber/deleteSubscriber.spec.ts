process.env.SUBSCRIBERS_TABLE = 'subscribers'
import {FakeDynamoDB} from '../mock/dynamoDB'
import {deleteSubscriber} from './deleteSubscriber'
import {DynamoDB} from '@aws-sdk/client-dynamodb'
import {
  APIGatewayProxyEvent,
  APIGatewayProxyResult
} from 'aws-lambda'


describe('DELETE /subscribers:', function () {
  const inputEvent: APIGatewayProxyEvent = {pathParameters: {email: 'some@body.com'}} as unknown as APIGatewayProxyEvent
  let fakeDynamoDB: FakeDynamoDB
  beforeEach(() => {
    fakeDynamoDB = new FakeDynamoDB()
  })

  describe('when an item exists', () => {
    const SUBKEY: { id: { S: string } } = {id: {S: '1'}}
    const SUB: { name: { S: string }; id: { S: string }; email: { S: string } } = {
      id: {S: '1'},
      name: {S: 'somebody'},
      email: {S: 'some@body.com'}
    }
    let result: APIGatewayProxyResult
    beforeEach(async () => {
      fakeDynamoDB._items.set(JSON.stringify(SUBKEY), SUB)
      result = await deleteSubscriber(fakeDynamoDB as unknown as DynamoDB)(inputEvent)
    })
    it('should delete from table "subscribers"', function () {
      expect(fakeDynamoDB.table).toEqual('subscribers')
      expect(fakeDynamoDB._items.size).toEqual(0)
    })
    it('should return 200', async () => {
      expect(result.statusCode).toEqual(200)
    })
  })

  describe('when more than one item exists', () => {
    const SUBKEY: { id: { S: string } } = {id: {S: '1'}}
    const SUBKEY2: { id: { S: string } } = {id: {S: '2'}}
    const SUB: { name: { S: string }; id: { S: string }; email: { S: string } } = {
      id: {S: '1'},
      name: {S: 'somebody'},
      email: {S: 'some@body.com'}
    }
    const SUB2: { name: { S: string }; id: { S: string }; email: { S: string } } = {
      id: {S: '2'},
      name: {S: 'somebody'},
      email: {S: 'some@body.com'}
    }
    let result: APIGatewayProxyResult
    beforeEach(async () => {
      fakeDynamoDB._items.set(JSON.stringify(SUBKEY), SUB)
      fakeDynamoDB._items.set(JSON.stringify(SUBKEY2), SUB2)
      expect(fakeDynamoDB._items.size).toEqual(2)
      result = await deleteSubscriber(fakeDynamoDB as unknown as DynamoDB)(inputEvent)
    })
    it('should not delete from table "subscribers"', function () {
      expect(fakeDynamoDB.table).toEqual('subscribers')
      expect(fakeDynamoDB._items.size).toEqual(2)
    })
    it('should return 500 error', async () => {
      expect(result.statusCode).toEqual(500)
      expect(JSON.parse(result.body).reason).toEqual("There was more than one existing entry with the specified email address. This is likely due to a technical problem. Please contact the organizers.")
    })
  })

  describe('when an item does not exist', () => {
    let result: APIGatewayProxyResult
    beforeEach(async () => {
      result = await deleteSubscriber(fakeDynamoDB as unknown as DynamoDB)(inputEvent)
    })


    it('should return 404 error', () => {
      expect(result.statusCode).toEqual(404)
      expect(JSON.parse(result.body).reason).toEqual("The specified email does not exist.")
    })
  })


})