import {
  APIGatewayProxyEvent,
  APIGatewayProxyResult
} from 'aws-lambda'
import {v4 as uuid} from 'uuid'
import {
  DynamoDB,
  PutItemInput
} from '@aws-sdk/client-dynamodb'
import {
  Subscriber,
  UUID
} from '../common/types'
import {
  failure,
  success,
  tableName
} from '../common/util'
import {SES} from '@aws-sdk/client-ses'

type SubscriberPayload = Subscriber & { key?: UUID, value?: any }

const valueType = (value: any) => {
  if (typeof value === 'string') {
    return 'S'
  }
  if (typeof value === 'number') {
    return 'N'
  }
  if (typeof value === 'boolean') {
    return 'BOOL'
  }
}

const toItemInput = (payload: SubscriberPayload) => {
  const {key, value} = payload
  const item = key !== undefined ? value : payload
  const Key = key !== undefined ? key : uuid()
  const Item = {id: {S: Key}}
  Object.entries(item).forEach(([key, value]) => Item[key] = {[valueType(value)]: value})
  return {TableName: tableName, Item}
}

async function persistSubscriber(parsed: any, dynamoDB: DynamoDB): Promise<void> {
  const input: PutItemInput = toItemInput(parsed)
  await dynamoDB.putItem(input)
}

async function sendConfirmationRequest({consentKey, email, name}: SubscriberPayload, ses: SES): Promise<void> {
  const TemplateData: string = `{"name":"${name}", "email":"${email}", "consentKey":"${consentKey}"}`
  const Template: string = process.env.CONFIRMATION_TEMPLATE
  const Source: string = process.env.SOURCE_EMAIL
  const Destination = {ToAddresses: [email]}
  console.log('Sending confirmation email\n\n %s\n%s\n%s\n%s\n\n', Template, Source, Destination, TemplateData)
  await ses.sendBulkTemplatedEmail({
    Template,
    Source,
    Destinations: [{
        Destination,
        ReplacementTemplateData: TemplateData
      }],
    DefaultTemplateData: TemplateData,
    ConfigurationSetName: "Standard"
  })

  console.log('Confirmation sent')
}

export const postSubscriber = (ses: SES, dynamoDB: DynamoDB) => async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
  try {
    const subscriber: SubscriberPayload = JSON.parse(event.body)
    subscriber.consentKey = uuid()
    const existing = await dynamoDB.scan({
      FilterExpression: 'email = :email',
      ExpressionAttributeValues: {':email': {S: subscriber.email}},
      TableName: tableName
    })
    if (!existing.Items || existing.Items.length === 0) {

      await persistSubscriber(subscriber, dynamoDB)
      await sendConfirmationRequest(subscriber, ses)

      return success()
    } else {
      return failure(400, 'A subscriber with the same email address already exists.')
    }
  } catch (e) {
    console.log('error while adding subscription: \n %o', e)
    return e.type === 'ValidationError'
           ? failure(400, 'The input arguments were invalid.')
           : failure(500, e.message)
  }
}