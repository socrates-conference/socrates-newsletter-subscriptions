import {FakeSES} from '../mock/ses'

process.env.SUBSCRIBERS_TABLE = 'subscribers'
import {APIGatewayProxyResult} from 'aws-lambda'
import {DynamoDB} from '@aws-sdk/client-dynamodb'
import {postSubscriber} from './postSubscriber'
import {FakeDynamoDB} from '../mock/dynamoDB'
import {SES} from '@aws-sdk/client-ses'


describe('POST /subscribers', () => {
  let result: APIGatewayProxyResult
  const dynamoDB: FakeDynamoDB = new FakeDynamoDB()
  const ses: FakeSES = new FakeSES()

  beforeAll(async () => {
    const event: any = {body: JSON.stringify({'name': 'something', 'email': 'some@body.com'})}
    result = await postSubscriber(ses as unknown as SES, dynamoDB as unknown as DynamoDB)(event)
  })

  it('should return status 200', async () => {
    expect(result.statusCode).toEqual(200)
  })

  it('should store a valid subscriber', async () => {
    expect(dynamoDB.table).toEqual('subscribers')
    expect((await dynamoDB.scan({TableName: process.env.SUBSCRIBERS_TABLE})).Items[0].name).toEqual({"S":"something"})
  })
})