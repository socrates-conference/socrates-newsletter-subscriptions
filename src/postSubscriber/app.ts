import {postSubscriber} from './postSubscriber'
import {DynamoDB} from '@aws-sdk/client-dynamodb'
import {SES} from '@aws-sdk/client-ses'

export const handler = postSubscriber(new SES({region:'eu-central-1'}), new DynamoDB({region: 'eu-central-1'}))